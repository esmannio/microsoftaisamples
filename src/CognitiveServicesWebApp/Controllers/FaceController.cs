﻿using CognitiveServices.Helper;
using CognitiveServices.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace CognitiveServices.Controllers
{
    public class FaceController : Controller
    {
        private AppSettings AppSettings { get; }

        public FaceController(IOptions<AppSettings> settings)
        {
            AppSettings = settings.Value;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(UploadImageModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { error = "invalid data" });
            }

            CognitiveApiClient cognitiveApiClient = new CognitiveApiClient(
                subscriptionKey: AppSettings.ApiKeys.FaceApiKey,
                apiRegion: "westeurope",
                apiVersion: "v1.0",
                apiName: "face");

            var r = await cognitiveApiClient.PostImage(
                apiMethod: "detect",
                requestParameters: "returnFaceId=true&returnFaceLandmarks=true&returnFaceAttributes=age,gender,headPose,smile,facialHair,glasses,emotion,hair,makeup,occlusion,accessories,blur,exposure,noise",
                base64StringContent: model.ImageData);

            return Json(r);
        }
    }
}