﻿using CognitiveServices.Helper;
using CognitiveServices.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace CognitiveServices.Controllers
{
    public class TextController : Controller
    {
        private AppSettings AppSettings { get; }

        public TextController(IOptions<AppSettings> settings)
        {
            AppSettings = settings.Value;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(UploadTextModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                CognitiveApiClient cognitiveApiClient = new CognitiveApiClient(
                subscriptionKey: AppSettings.ApiKeys.TextApiKey,
                apiRegion: "westeurope",
                apiVersion: "v2.0",
                apiName: "text/analytics");

                var r = await cognitiveApiClient.PostJson(
                    apiMethod: "keyPhrases",
                    requestParameters: null,
                    body: new
                    {
                        documents = new object[]{
                            new {
                                id ="1",
                                language = model.Language,
                                text = model.Text
                            },
                        }
                    });

                model.Response = r.ToString();
                return View(model);
            }
            catch (Exception ex)
            {
                model.Response = ex.Message;
                return View(model);
            }
        }

        public IActionResult Sentiment()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Sentiment(UploadTextModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            try
            {
                CognitiveApiClient cognitiveApiClient = new CognitiveApiClient(
                subscriptionKey: AppSettings.ApiKeys.TextApiKey,
                apiRegion: "Westeurope",
                apiVersion: "v2.0",
                apiName: "text/analytics");

                var r = await cognitiveApiClient.PostJson(
                    apiMethod: "Sentiment",
                    requestParameters: null,
                    body: new
                    {
                        documents = new object[]{
                            new {
                                id ="1",
                                language = model.Language,
                                text = model.Text
                            },
                        }
                    });

                model.Response = r.ToString();
                return View(model);
            }
            catch (Exception ex)
            {
                model.Response = ex.Message;
                return View(model);
            }
        }
    }
}