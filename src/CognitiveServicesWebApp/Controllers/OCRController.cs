﻿using CognitiveServices.Helper;
using CognitiveServices.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System;
using System.Threading.Tasks;

namespace CognitiveServices.Controllers
{
    public class OCRController : Controller
    {
        private AppSettings AppSettings { get; }

        public OCRController(IOptions<AppSettings> settings)
        {
            AppSettings = settings.Value;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(UploadImageModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { error = "invalid data" });
            }

            try
            {
                CognitiveApiClient cognitiveApiClient = new CognitiveApiClient(
                    subscriptionKey: AppSettings.ApiKeys.VisionApiKey,
                    apiRegion: "westeurope",
                    apiVersion: "v1.0",
                    apiName: "vision");

                var r = await cognitiveApiClient.PostImage(
                    apiMethod:"ocr", 
                    requestParameters: "language = unk & detectOrientation = true",
                    base64StringContent: model.ImageData);

                return Json(r);
            }
            catch (Exception ex)
            {
                return Json(new { error = ex.Message });
            }
        }
    }
}