﻿using CognitiveServices.Helper;
using CognitiveServices.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;

namespace CognitiveServices.Controllers
{
    public class VisionController : Controller
    {
        private AppSettings AppSettings { get; }

        public VisionController(IOptions<AppSettings> settings)
        {
            AppSettings = settings.Value;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Index(UploadImageModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { error = "invalid data" });
            }

            CognitiveApiClient cognitiveApiClient = new CognitiveApiClient(
               subscriptionKey: AppSettings.ApiKeys.VisionApiKey,
               apiRegion: "westeurope",
               apiVersion: "v1.0",
               apiName: "vision");

            // https://westeurope.dev.cognitive.microsoft.com/docs/services/56f91f2d778daf23d8ec6739/operations/56f91f2e778daf14a499e1fa
            var r = await cognitiveApiClient.PostImage(
                apiMethod: "analyze",
                requestParameters: "visualFeatures=Categories,Tags,Description,Faces,ImageType,Color,Adult&details=Celebrities,Landmarks&language=en",
                base64StringContent: model.ImageData);

            return Json(r);
        }
    }
}