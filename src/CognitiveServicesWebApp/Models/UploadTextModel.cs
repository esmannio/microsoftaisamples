﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CognitiveServices.Models
{
    public class UploadTextModel
    {
        [Required]
        public string Text { get; set; }

        [Required]
        public string Language { get; set; }

       
        public string Response { get; set; }

    }
}
