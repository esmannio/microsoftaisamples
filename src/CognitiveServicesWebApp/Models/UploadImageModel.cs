﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace CognitiveServices.Models
{
    public class UploadImageModel
    {
        [Required]
        public string ImageData { get; set; }
    }
}
