﻿namespace CognitiveServices
{
    public class AppSettings
    {
        public ApiKeys ApiKeys { get; set; }
    }

    public class ApiKeys
    {
        public string FaceApiKey { get; set; }
        public string VisionApiKey { get; set; }
        public string TextApiKey { get; set; }
    }
}