﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Net.Http.Headers;

namespace CognitiveServices.Helper
{
    public class CognitiveApiClient
    {
        private HttpClient client { get; set; }

        public CognitiveApiClient(string subscriptionKey, string apiRegion, string apiVersion, string apiName) : this(new HttpClient(), subscriptionKey, apiRegion, apiVersion, apiName)
        {
        }

        public CognitiveApiClient(HttpClient httpClient, string subscriptionKey, string apiRegion, string apiVersion, string apiName)
        {
            client = httpClient;
            client.BaseAddress = new Uri($"https://{apiRegion}.api.cognitive.microsoft.com/{apiName}/{apiVersion}/");
            client.DefaultRequestHeaders.Add("Ocp-Apim-Subscription-Key", subscriptionKey);
        }

        public async Task<JToken> PostImage(string apiMethod, string requestParameters, string base64StringContent)
        {
            JToken response = null;
            using (ByteArrayContent content = new ByteArrayContent(Convert.FromBase64String(base64StringContent)))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                response = await PostByteArrayContent(apiMethod, requestParameters, content);
            }

            return response;
        }

        public async Task<JToken> PostImage(string apiMethod, string requestParameters, Stream steamContent)
        {
            byte[] buffer = null;
            if (steamContent is MemoryStream)
            {
                buffer = ((MemoryStream)steamContent).ToArray();
            }
            else
            {
                MemoryStream mem = new MemoryStream();
                steamContent.CopyTo(mem);
                using (mem)
                {
                    buffer = mem.ToArray();
                }
            }

            JToken response = null;
            using (ByteArrayContent content = new ByteArrayContent(buffer))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                response = await PostByteArrayContent(apiMethod, requestParameters, content);
            }

            return response;
        }

        public async Task<JToken> PostByteArrayContent(string apiMethod, string requestParameters, ByteArrayContent content)
        {
            return await Post(apiMethod, requestParameters, content);
        }

        public async Task<JToken> PostJson(string apiMethod, string requestParameters, object body)
        {
            return await PostJson(apiMethod, requestParameters, JToken.FromObject(body).ToString());
        }
        public async Task<JToken> PostJson(string apiMethod, string requestParameters, string json)
        {
            JToken response = null;
            using (StringContent content = new StringContent(json))
            {
                content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                response = await Post(apiMethod, requestParameters, content);
            }

            return response;
        }

        public async Task<JToken> PostStringContent(string apiMethod, string requestParameters, StringContent content)
        {
            return await Post(apiMethod, requestParameters, content);
        }

        public async Task<JToken> Post(string apiMethod, string requestParameters, HttpContent content)
        {
            try
            {
                string uri = requestParameters != null ? $"{apiMethod}?{requestParameters}" : apiMethod;
                // Execute the REST API call.
                var response = await client.PostAsync(uri, content);

                if (response.IsSuccessStatusCode)
                {
                    string json = null;
                    using (response)
                    {
                        json = await response.Content.ReadAsStringAsync();
                    }

                    return JToken.Parse(json);
                }
                else
                {
                    return JToken.FromObject(new { response.StatusCode, response.ReasonPhrase, response.IsSuccessStatusCode });
                }
            }
            catch (Exception ex)
            {
                return JToken.FromObject(new { ex.Message, apiMethod, requestParameters });
            }
        }
    }
}
