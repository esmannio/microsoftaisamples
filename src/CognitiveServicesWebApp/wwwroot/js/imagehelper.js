﻿const player = document.getElementById('player');
const canvas = document.getElementById('canvas');
const captureButton = document.getElementById('capture');
const responseText = document.getElementById('responseText');
const sendButton = document.getElementById('send');
const selectList = document.getElementById('videoSource');

navigator.mediaDevices.enumerateDevices()
    .then(gotDevices);

function gotDevices(deviceInfos) {
    if (selectList.hasChildNodes() === false) {
        for (var i = 0; i !== deviceInfos.length; ++i) {
            var deviceInfo = deviceInfos[i];
            var option = document.createElement('option');
            option.value = deviceInfo.deviceId;
            console.log('deviceInfo.deviceId: ', deviceInfo);
            if (deviceInfo.kind === 'audioinput') {
                // ignore audio
            }
            else if (deviceInfo.kind === 'videoinput') {
                option.text = deviceInfo.label || 'camera ' + (selectList.length + 1);
                selectList.appendChild(option);
            } else {
                console.log('Found one other kind of source/device: ', deviceInfo);
            }
        }
    }
}

function getStream() {
    if (window.stream) {
        window.stream.getTracks().forEach(function (track) {
            track.stop();
        });
    }

    var constraints = {
        audio: false, // ignore audio
        video: {
            deviceId: { exact: selectList.value }
        }
    };

    navigator.mediaDevices.getUserMedia(constraints).
        then(gotStream).catch(handleError);
}

function gotStream(stream) {
    window.stream = stream; // make stream available to console
    player.srcObject = stream;
}

function handleError(error) {
    console.log('Error: ', error);
}

captureButton.addEventListener('click', () => {
    // Draw the video frame to the canvas.
    responseText.value = "";
    canvas.width = player.clientWidth;
    canvas.height = player.clientHeight;
    canvas.getContext('2d').drawImage(player, 0, 0, canvas.width, canvas.height);
});

selectList.addEventListener('change', () => {
    player.srcObject.getVideoTracks().forEach(track => track.stop());
    player.srcObject = null;
    console.log("selectList: changed");
});

sendButton.addEventListener('click', () => {

    var base64ImageString = canvas.toDataURL("image/png").split(",").pop();
    var form = new FormData();
    form.append("model.ImageData", base64ImageString);

    $.ajax({
        url: window.location.href,

        beforeSend: function (xhrObj) {
            // Request headers.
            //xhrObj.setRequestHeader("Content-Type", "...");
        },
        contentType: false,
        processData: false,
        type: "POST",
        data: form
    })
        .done(function (data) {
            // Show formatted JSON on webpage.
            console.log(JSON.stringify(data, null, 2));
            responseText.value = JSON.stringify(data, null, 2);
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            // Display error message.
            var errorString = (errorThrown === "") ? "Error. " : errorThrown + " (" + jqXHR.status + "): ";
            errorString += (jqXHR.responseText === "") ? "" : (jQuery.parseJSON(jqXHR.responseText).message) ?
                jQuery.parseJSON(jqXHR.responseText).message : jQuery.parseJSON(jqXHR.responseText).error.message;
            console.log(errorString);
        });
});

player.addEventListener('pause', () => {
    // Stop all video streams.
    player.srcObject.getVideoTracks().forEach(track => track.stop());
    player.srcObject = null;
    console.log("mediaControl: pause");
});

player.addEventListener('play', () => {
    // Attach the video stream to the video element and autoplay.
    navigator.mediaDevices.enumerateDevices()
        .then(gotDevices)
        .then(getStream)
        .catch(handleError);

    console.log("mediaControl: play");
});

player.addEventListener('click', () => {
    responseText.value = "";
    canvas.getContext('2d').drawImage(player, 0, 0, canvas.width, canvas.height);
    console.log("mediaControl: click");
});
